# Makefile for nm-stub debian package
TAG=nmstub:latest

.PHONY: local
local:
	equivs-build nm-stub

.PHONY: ondocker
ondocker:
	docker build -f Containerfile -t ${TAG} \
		--build-arg USER_ID=$$(id -u) --build-arg GROUP_ID=$$(id -g) .
	docker run --rm -v $${PWD}:/build ${TAG}
	docker rmi ${TAG}
	-dpkg-deb --info *.deb
