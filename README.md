# nm-stub

Builder for stub for network-manager-gnome.


## Usage

Either:

- Download the `.deb` directly from <https://gitlab.com/victorklos/nm-stub/-/jobs/artifacts/master/download?job=build>
- Do `make local`. You must have installed the `equivs` package
- Or `make ondocker` to build the `.deb` using docker.

In all cases the resulting `.deb` can be installed after which you can
`sudo apt purge network-manager network-manager-gnome`.


## Rationale

See the file `nm-stub` which explains why. In short: currently in Debian buster
it is impossible to purge network-manager in Gnome or Cinnamon. Meh.
